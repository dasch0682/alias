## structure (using bash)

1. `cd ${where.you.want.to.clone.this.repo}`
1. `git clone ${this_repo}`
2. `ln -s ${where.you.want.to.clone.this.repo}/alias/ ~/.alias_tree`
3. in profile (eg. `~/.bash_profile`), add this line
```
. ~/.alias_tree/init
```
4. add as many aliases as you wish to `~/.alias_tree/*.alias` files 
(create as many you want)
5. Notes :

    - any `${alias_file_name}.alias` has a dedicated vi edit associated alias
    (can be changed to any other editor in `~/.alias_tree/init` file), that is :
    `${alias_file_name}-alias`
    - a function provides all aliases grouped by alias file : `all-alias`
    - a dedicated alias gets you inside `~/.alias_tree` : galias
    - a few utility functions are defined in ~/.alias_tree/init
